move = key_left + key_right;
hsp = move*movespeed;

move_wrap(true, false, sprite_width-16);

if(vsp<10)vsp+=grav;

//kill by player

if (place_meeting(x,y-1,obj_player)){
    if (obj_player.y < y){
        with (obj_player){
         vsp = -jumpspeed;
         kill_count++;
         }
        score = score + 10;
        audio_play_sound(snd_hit,10,false);        
        instance_destroy();
    }        
}

//colission whit map
if(place_meeting(x,y+1,obj_floor) || place_meeting(x,y+1,obj_base_1))
{

    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 move_wrap(true, false, sprite_width-16);
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){

        hsp_jump_applied = sign(move);  
   
    }

    //don't jump horizontal
  if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_floor) || place_meeting(x+hsp,y,obj_base_1))
{
    while(!place_meeting(x+sign(hsp),y,obj_base_1))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_floor) || place_meeting(x,y+vsp,obj_base_1))
{
    while(!place_meeting(x, sign(vsp) + y,obj_floor) && !place_meeting(x, sign(vsp) + y,obj_base_1))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;


