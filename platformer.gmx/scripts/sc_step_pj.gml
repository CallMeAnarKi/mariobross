move = key_left + key_right;
hsp = move*movespeed;

if (key_jump = true){
audio_play_sound(snd_jump,10,false);
}

move_wrap(true, false, sprite_width-16);
if(vsp<10)vsp+=grav;


//kill by enemy
if(place_meeting(x+1,y,obj_joe_iz) || place_meeting(x+1,y,obj_joe_de) || place_meeting(x-1,y,obj_joe_iz) || place_meeting(x-1,y,obj_joe_de) || place_meeting(x,y-1,obj_joe_iz) || place_meeting(x,y-1,obj_joe_de) ||
   place_meeting(x+1,y,obj_seboia_iz) || place_meeting(x+1,y,obj_seboia_de) || place_meeting(x-1,y,obj_seboia_iz) || place_meeting(x-1,y,obj_seboia_de) || place_meeting(x,y-1,obj_seboia_iz) || place_meeting(x,y-1,obj_seboia_de) ||
   place_meeting(x+1,y,obj_judia_iz) || place_meeting(x+1,y,obj_judia_de) || place_meeting(x-1,y,obj_judia_iz) || place_meeting(x-1,y,obj_judia_de) || place_meeting(x,y-1,obj_judia_iz) || place_meeting(x,y-1,obj_judia_de) ){
 lives = lives -1;
 x = 32;
 y = 432;
}

//pass lvl by kill
if (kill_count == 6){
room_goto_next();
}

//pass lvl by money
if (money_count == 15){
room_goto_next();
}




// change sprite when move
if (hsp = 0 && grounded){
        sprite_index = spr_player_stand;
}
else {
    if (hsp !=0 && key_right != 0 && grounded){
        move_wrap(true, false, sprite_width-16);
        sprite_index = spr_player_run_right;
    }
    else{
        move_wrap(true, false, sprite_width-16);
        sprite_index = spr_player_run_left;
    }        
}


if(place_meeting(x,y+1,obj_floor) || place_meeting(x,y+1,obj_base_1)){

    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;        
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }
    
    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 move_wrap(true, false, sprite_width-16);
 hkp_count=0;
}

//Check jumping
if(jumping){

    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);  
    }

   //don't jump horizontal
    if(hkp_count < hkp_count_small){
        hsp = 0;
        sprite_index = spr_player_stand;

    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
        //changue sprite when jump
        if (hsp > 0 ){
            sprite_index = spr_player_jump_right;            
        }else if (hsp < 0){
            sprite_index = spr_player_jump_left;
        }
        
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big;
        if (hsp > 0){
            sprite_index = spr_player_jump_right;
        }else if (hsp < 0){
            sprite_index = spr_player_jump_left;
        }
    }
}



//horizontal collision
if(place_meeting(x+hsp,y,obj_floor) || place_meeting(x+hsp,y,obj_base_1) ||  place_meeting(x+hsp,y,obj_rekt))
{
    while(!place_meeting(x+sign(hsp),y,obj_base_1) && !place_meeting(x+sign(hsp),y,obj_rekt))
    {
        x+= sign(hsp);
    }
    hsp = 0;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,obj_floor) || place_meeting(x,y+vsp,obj_base_1) || place_meeting(x,y+vsp,obj_rekt))
{
    while(!place_meeting(x, sign(vsp) + y,obj_floor) && !place_meeting(x, sign(vsp) + y,obj_base_1) &&  !place_meeting(x, sign(vsp) + y,obj_rekt))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;

